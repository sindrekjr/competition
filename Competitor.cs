using System.Collections.Generic;

namespace CompetitionManager
{
    public class Competitor : Person
    {
        public virtual Coach Coach { get; set; }
        public int CoachId { get; set; }
        public virtual List<CompetitorSkill> Skills { get; set; }
        public virtual Team Team { get; set; }
        public int TeamId { get; set; }
    }
}