using Microsoft.EntityFrameworkCore;

namespace CompetitionManager.Database
{
    public class CompetitionContext : DbContext
    {
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Team> Teams { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=Competitions;Trusted_Connection=true;");
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            CompetitionSeed.Seed(modelBuilder);
        }
    }
}