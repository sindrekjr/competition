using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

namespace CompetitionManager.Database
{
    static class CompetitionSeed
    {
        static IEnumerable<Competitor> competitors;
        static IEnumerable<Team> teams;
        static IEnumerable<Skill> skills;
        static IEnumerable<Coach> coaches;
        static IEnumerable<CompetitorSkill> competitorSkills;
        

        public static void Seed(ModelBuilder modelBuilder)
        {
            teams = Teams();
            skills = Skills();
            coaches = Coaches();
            competitors = Competitors();
            competitorSkills = CompetitorSkills();
            
            modelBuilder.Entity<CompetitorSkill>().HasKey(cs => new { cs.CompetitorId, cs.SkillId });
            modelBuilder.Entity<Competitor>().HasData(competitors);
            modelBuilder.Entity<Coach>().HasData(coaches);
            modelBuilder.Entity<Skill>().HasData(skills);
            modelBuilder.Entity<CompetitorSkill>().HasData(competitorSkills);
            modelBuilder.Entity<Team>().HasData(teams);
        }

        static IEnumerable<Skill> Skills() => Enum.GetValues(typeof(Group)).Cast<Group>().Skip(1).Select(g => new Skill { Id = (int) g });

        static IEnumerable<Coach> Coaches()
        {
            Random rnd = new Random();
            int i = 1;

            yield return new Coach { Id = i++, FirstName = "Steve", TrainingModifier = rnd.Next(10) };
            yield return new Coach { Id = i++, FirstName = "Atlas", TrainingModifier = rnd.Next(10) };
            yield return new Coach { Id = i++, FirstName = "Greg", TrainingModifier = rnd.Next(10) };
            yield return new Coach { Id = i++, FirstName = "Dean", TrainingModifier = rnd.Next(10) };
        }

        static IEnumerable<Competitor> Competitors()
        {
            int coachCount = coaches.Count();
            int teamCount = teams.Count();
            int i = 1;

            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Gordon", LastName = "Freeman", Gender = Gender.Male };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Solid Snake", Gender = Gender.Male };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Maximus", LastName = "Decimus Meridius", Gender = Gender.Male };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Jim", LastName = "Halpert", Gender = Gender.Male };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Sander", LastName = "Cohen", Gender = Gender.Male };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Abysswalker Artorias", Gender = Gender.Male };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Sekiro", Gender = Gender.Male };

            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Farah", Gender = Gender.Female };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Alyx", LastName = "Vance", Gender = Gender.Female };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Lucatiel", Gender = Gender.Female };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Quelana", Gender = Gender.Female };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Caelar", LastName = "Argent", Gender = Gender.Female };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Olishilelia", Gender = Gender.Female };
            yield return new Competitor { Id = i, CoachId = (i % coachCount) + 1, TeamId = (i++ % teamCount) + 1, FirstName = "Bellatrix", LastName = "Lestrange", Gender = Gender.Female };
        }

        static IEnumerable<CompetitorSkill> CompetitorSkills()
        {
            foreach(var c in competitors)
            {
                Random rnd = new Random();
                var randomSkills = skills.OrderBy(s => rnd.Next()).Take(3).ToList();

                yield return new CompetitorSkill { CompetitorId = c.Id, SkillId = randomSkills[0].Id };
                yield return new CompetitorSkill { CompetitorId = c.Id, SkillId = randomSkills[1].Id };
                yield return new CompetitorSkill { CompetitorId = c.Id, SkillId = randomSkills[2].Id };
            }
        }

        static IEnumerable<Team> Teams()
        {
            yield return new Team { Id = 1, Name = "Team Good" };
            yield return new Team { Id = 2, Name = "Team Evil" };
        }
    }
}