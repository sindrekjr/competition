namespace CompetitionManager
{
    public class CompetitorSkill
    {
        public virtual Competitor Competitor { get; set; }
        public int CompetitorId { get; set; }
        
        public virtual Skill Skill { get; set; }
        public int SkillId { get; set; }
    }
}