namespace CompetitionManager
{
    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get => $"{FirstName} {LastName}"; }
        public Gender Gender { get; set; }
        public int Age { get ; set; }
    }
}