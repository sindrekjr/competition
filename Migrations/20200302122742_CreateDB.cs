﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetitionManager.Migrations
{
    public partial class CreateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Coaches",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    TrainingModifier = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coaches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Competitors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    CoachId = table.Column<int>(nullable: false),
                    TeamId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Competitors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Competitors_Coaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Competitors_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompetitorSkill",
                columns: table => new
                {
                    CompetitorId = table.Column<int>(nullable: false),
                    SkillId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitorSkill", x => new { x.CompetitorId, x.SkillId });
                    table.ForeignKey(
                        name: "FK_CompetitorSkill_Competitors_CompetitorId",
                        column: x => x.CompetitorId,
                        principalTable: "Competitors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompetitorSkill_Skills_SkillId",
                        column: x => x.SkillId,
                        principalTable: "Skills",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Coaches",
                columns: new[] { "Id", "Age", "FirstName", "Gender", "LastName", "TrainingModifier" },
                values: new object[,]
                {
                    { 1, 0, "Steve", 0, null, 4 },
                    { 2, 0, "Atlas", 0, null, 9 },
                    { 3, 0, "Greg", 0, null, 2 },
                    { 4, 0, "Dean", 0, null, 7 }
                });

            migrationBuilder.InsertData(
                table: "Skills",
                column: "Id",
                values: new object[]
                {
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Team Good" },
                    { 2, "Team Evil" }
                });

            migrationBuilder.InsertData(
                table: "Competitors",
                columns: new[] { "Id", "Age", "CoachId", "FirstName", "Gender", "LastName", "TeamId" },
                values: new object[,]
                {
                    { 2, 0, 3, "Solid Snake", 1, null, 1 },
                    { 4, 0, 1, "Jim", 1, "Halpert", 1 },
                    { 6, 0, 3, "Abysswalker Artorias", 1, null, 1 },
                    { 8, 0, 1, "Farah", 2, null, 1 },
                    { 10, 0, 3, "Lucatiel", 2, null, 1 },
                    { 12, 0, 1, "Caelar", 2, "Argent", 1 },
                    { 14, 0, 3, "Bellatrix", 2, "Lestrange", 1 },
                    { 1, 0, 2, "Gordon", 1, "Freeman", 2 },
                    { 3, 0, 4, "Maximus", 1, "Decimus Meridius", 2 },
                    { 5, 0, 2, "Sander", 1, "Cohen", 2 },
                    { 7, 0, 4, "Sekiro", 1, null, 2 },
                    { 9, 0, 2, "Alyx", 2, "Vance", 2 },
                    { 11, 0, 4, "Quelana", 2, null, 2 },
                    { 13, 0, 2, "Olishilelia", 2, null, 2 }
                });

            migrationBuilder.InsertData(
                table: "CompetitorSkill",
                columns: new[] { "CompetitorId", "SkillId" },
                values: new object[,]
                {
                    { 2, 1 },
                    { 1, 3 },
                    { 3, 4 },
                    { 3, 7 },
                    { 3, 5 },
                    { 5, 7 },
                    { 5, 1 },
                    { 5, 5 },
                    { 7, 5 },
                    { 7, 3 },
                    { 7, 8 },
                    { 9, 5 },
                    { 9, 6 },
                    { 9, 7 },
                    { 11, 2 },
                    { 11, 6 },
                    { 11, 5 },
                    { 13, 3 },
                    { 1, 6 },
                    { 1, 1 },
                    { 14, 2 },
                    { 14, 1 },
                    { 2, 7 },
                    { 2, 3 },
                    { 4, 3 },
                    { 4, 4 },
                    { 4, 8 },
                    { 6, 5 },
                    { 6, 3 },
                    { 6, 7 },
                    { 13, 5 },
                    { 8, 3 },
                    { 8, 1 },
                    { 10, 4 },
                    { 10, 5 },
                    { 10, 6 },
                    { 12, 3 },
                    { 12, 2 },
                    { 12, 5 },
                    { 14, 7 },
                    { 8, 4 },
                    { 13, 7 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Competitors_CoachId",
                table: "Competitors",
                column: "CoachId");

            migrationBuilder.CreateIndex(
                name: "IX_Competitors_TeamId",
                table: "Competitors",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetitorSkill_SkillId",
                table: "CompetitorSkill",
                column: "SkillId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompetitorSkill");

            migrationBuilder.DropTable(
                name: "Competitors");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "Coaches");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
