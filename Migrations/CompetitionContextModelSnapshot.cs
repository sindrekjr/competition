﻿// <auto-generated />
using CompetitionManager.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CompetitionManager.Migrations
{
    [DbContext(typeof(CompetitionContext))]
    partial class CompetitionContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CompetitionManager.Coach", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Age")
                        .HasColumnType("int");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Gender")
                        .HasColumnType("int");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TrainingModifier")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Coaches");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Age = 0,
                            FirstName = "Steve",
                            Gender = 0,
                            TrainingModifier = 4
                        },
                        new
                        {
                            Id = 2,
                            Age = 0,
                            FirstName = "Atlas",
                            Gender = 0,
                            TrainingModifier = 9
                        },
                        new
                        {
                            Id = 3,
                            Age = 0,
                            FirstName = "Greg",
                            Gender = 0,
                            TrainingModifier = 2
                        },
                        new
                        {
                            Id = 4,
                            Age = 0,
                            FirstName = "Dean",
                            Gender = 0,
                            TrainingModifier = 7
                        });
                });

            modelBuilder.Entity("CompetitionManager.Competitor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Age")
                        .HasColumnType("int");

                    b.Property<int>("CoachId")
                        .HasColumnType("int");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Gender")
                        .HasColumnType("int");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CoachId");

                    b.HasIndex("TeamId");

                    b.ToTable("Competitors");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Age = 0,
                            CoachId = 2,
                            FirstName = "Gordon",
                            Gender = 1,
                            LastName = "Freeman",
                            TeamId = 2
                        },
                        new
                        {
                            Id = 2,
                            Age = 0,
                            CoachId = 3,
                            FirstName = "Solid Snake",
                            Gender = 1,
                            TeamId = 1
                        },
                        new
                        {
                            Id = 3,
                            Age = 0,
                            CoachId = 4,
                            FirstName = "Maximus",
                            Gender = 1,
                            LastName = "Decimus Meridius",
                            TeamId = 2
                        },
                        new
                        {
                            Id = 4,
                            Age = 0,
                            CoachId = 1,
                            FirstName = "Jim",
                            Gender = 1,
                            LastName = "Halpert",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 5,
                            Age = 0,
                            CoachId = 2,
                            FirstName = "Sander",
                            Gender = 1,
                            LastName = "Cohen",
                            TeamId = 2
                        },
                        new
                        {
                            Id = 6,
                            Age = 0,
                            CoachId = 3,
                            FirstName = "Abysswalker Artorias",
                            Gender = 1,
                            TeamId = 1
                        },
                        new
                        {
                            Id = 7,
                            Age = 0,
                            CoachId = 4,
                            FirstName = "Sekiro",
                            Gender = 1,
                            TeamId = 2
                        },
                        new
                        {
                            Id = 8,
                            Age = 0,
                            CoachId = 1,
                            FirstName = "Farah",
                            Gender = 2,
                            TeamId = 1
                        },
                        new
                        {
                            Id = 9,
                            Age = 0,
                            CoachId = 2,
                            FirstName = "Alyx",
                            Gender = 2,
                            LastName = "Vance",
                            TeamId = 2
                        },
                        new
                        {
                            Id = 10,
                            Age = 0,
                            CoachId = 3,
                            FirstName = "Lucatiel",
                            Gender = 2,
                            TeamId = 1
                        },
                        new
                        {
                            Id = 11,
                            Age = 0,
                            CoachId = 4,
                            FirstName = "Quelana",
                            Gender = 2,
                            TeamId = 2
                        },
                        new
                        {
                            Id = 12,
                            Age = 0,
                            CoachId = 1,
                            FirstName = "Caelar",
                            Gender = 2,
                            LastName = "Argent",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 13,
                            Age = 0,
                            CoachId = 2,
                            FirstName = "Olishilelia",
                            Gender = 2,
                            TeamId = 2
                        },
                        new
                        {
                            Id = 14,
                            Age = 0,
                            CoachId = 3,
                            FirstName = "Bellatrix",
                            Gender = 2,
                            LastName = "Lestrange",
                            TeamId = 1
                        });
                });

            modelBuilder.Entity("CompetitionManager.CompetitorSkill", b =>
                {
                    b.Property<int>("CompetitorId")
                        .HasColumnType("int");

                    b.Property<int>("SkillId")
                        .HasColumnType("int");

                    b.HasKey("CompetitorId", "SkillId");

                    b.HasIndex("SkillId");

                    b.ToTable("CompetitorSkill");

                    b.HasData(
                        new
                        {
                            CompetitorId = 1,
                            SkillId = 1
                        },
                        new
                        {
                            CompetitorId = 1,
                            SkillId = 6
                        },
                        new
                        {
                            CompetitorId = 1,
                            SkillId = 3
                        },
                        new
                        {
                            CompetitorId = 2,
                            SkillId = 1
                        },
                        new
                        {
                            CompetitorId = 2,
                            SkillId = 7
                        },
                        new
                        {
                            CompetitorId = 2,
                            SkillId = 3
                        },
                        new
                        {
                            CompetitorId = 3,
                            SkillId = 4
                        },
                        new
                        {
                            CompetitorId = 3,
                            SkillId = 7
                        },
                        new
                        {
                            CompetitorId = 3,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 4,
                            SkillId = 3
                        },
                        new
                        {
                            CompetitorId = 4,
                            SkillId = 4
                        },
                        new
                        {
                            CompetitorId = 4,
                            SkillId = 8
                        },
                        new
                        {
                            CompetitorId = 5,
                            SkillId = 7
                        },
                        new
                        {
                            CompetitorId = 5,
                            SkillId = 1
                        },
                        new
                        {
                            CompetitorId = 5,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 6,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 6,
                            SkillId = 3
                        },
                        new
                        {
                            CompetitorId = 6,
                            SkillId = 7
                        },
                        new
                        {
                            CompetitorId = 7,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 7,
                            SkillId = 3
                        },
                        new
                        {
                            CompetitorId = 7,
                            SkillId = 8
                        },
                        new
                        {
                            CompetitorId = 8,
                            SkillId = 3
                        },
                        new
                        {
                            CompetitorId = 8,
                            SkillId = 4
                        },
                        new
                        {
                            CompetitorId = 8,
                            SkillId = 1
                        },
                        new
                        {
                            CompetitorId = 9,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 9,
                            SkillId = 6
                        },
                        new
                        {
                            CompetitorId = 9,
                            SkillId = 7
                        },
                        new
                        {
                            CompetitorId = 10,
                            SkillId = 4
                        },
                        new
                        {
                            CompetitorId = 10,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 10,
                            SkillId = 6
                        },
                        new
                        {
                            CompetitorId = 11,
                            SkillId = 2
                        },
                        new
                        {
                            CompetitorId = 11,
                            SkillId = 6
                        },
                        new
                        {
                            CompetitorId = 11,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 12,
                            SkillId = 3
                        },
                        new
                        {
                            CompetitorId = 12,
                            SkillId = 2
                        },
                        new
                        {
                            CompetitorId = 12,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 13,
                            SkillId = 3
                        },
                        new
                        {
                            CompetitorId = 13,
                            SkillId = 5
                        },
                        new
                        {
                            CompetitorId = 13,
                            SkillId = 7
                        },
                        new
                        {
                            CompetitorId = 14,
                            SkillId = 7
                        },
                        new
                        {
                            CompetitorId = 14,
                            SkillId = 1
                        },
                        new
                        {
                            CompetitorId = 14,
                            SkillId = 2
                        });
                });

            modelBuilder.Entity("CompetitionManager.Skill", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.HasKey("Id");

                    b.ToTable("Skills");

                    b.HasData(
                        new
                        {
                            Id = 1
                        },
                        new
                        {
                            Id = 2
                        },
                        new
                        {
                            Id = 3
                        },
                        new
                        {
                            Id = 4
                        },
                        new
                        {
                            Id = 5
                        },
                        new
                        {
                            Id = 6
                        },
                        new
                        {
                            Id = 7
                        },
                        new
                        {
                            Id = 8
                        });
                });

            modelBuilder.Entity("CompetitionManager.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Teams");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Team Good"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Team Evil"
                        });
                });

            modelBuilder.Entity("CompetitionManager.Competitor", b =>
                {
                    b.HasOne("CompetitionManager.Coach", "Coach")
                        .WithMany()
                        .HasForeignKey("CoachId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CompetitionManager.Team", "Team")
                        .WithMany("Competitors")
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("CompetitionManager.CompetitorSkill", b =>
                {
                    b.HasOne("CompetitionManager.Competitor", "Competitor")
                        .WithMany("Skills")
                        .HasForeignKey("CompetitorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CompetitionManager.Skill", "Skill")
                        .WithMany()
                        .HasForeignKey("SkillId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
