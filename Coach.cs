namespace CompetitionManager
{
    public class Coach : Person
    {
        public int TrainingModifier { get; set; }
    }
}