﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using CompetitionManager.Database;

namespace CompetitionManager
{
    static class Program
    {
        private static readonly Dictionary<string, Func<string, bool>> commands =
            new Dictionary<string, Func<string, bool>>
            {
                ["add!"] = Add,
                ["get!"] = Get,
                ["help!"] = PrintHelp,
                ["exit!"] = Quit,
                ["quit!"] = Quit
            };

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Competition Manager! Type \"help!\" to get some help.");

            do Console.Write("\n> ");
            while(ProcessCommand());
        }

        static bool ProcessCommand() => ProcessCommand(Console.ReadLine());
        static bool ProcessCommand(string input) => commands[input.Split(' ')[0].ToLower()](input.ToLower());

        static bool Add(string input)
        {
            using(var DbContext = new CompetitionContext())
            {
                string[] names = input.StripCmd(Add).Trim().Split(" ");

                DbContext.Competitors.Add(new Competitor
                {
                    FirstName = names[0],
                    LastName = (names.Length > 1) ? string.Join(" ", names.Skip(1)) : ""
                });

                DbContext.SaveChanges();
            }

            return true;
        }

        static bool Get(string input)
        {
            using(var DbContext = new CompetitionContext())
            {
                string name = input.StripCmd(Get);
                var result = (name.Length == 0) 
                    ? DbContext.Competitors 
                    : DbContext.Competitors.Where(c => c.FirstName.ToLower().Contains(name) ||  c.LastName.ToLower().Contains(name));
                
                foreach(Competitor c in result) Console.WriteLine(c.Name);
            }

            return true;
        }

        static bool PrintHelp(string input)
        {
            Console.WriteLine("Available commands:");
            foreach(string command in commands.Keys) Console.WriteLine($"* {command}");

            return true;
        }

        static bool Quit(string input) => false;

        public static string StripCmd(this string input, Func<string, bool> cmd) => input.Replace(commands.First(c => c.Value == cmd).Key, "").Trim();
    }
}
