using System.Collections.Generic;

namespace CompetitionManager
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Competitor> Competitors { get; set; }
    }
}