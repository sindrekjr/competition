using System;
using System.Reflection;

namespace CompetitionManager
{
    [Flags]
    public enum Group
    {
        None,
        [StringValue("Bow Hunting")]BowHunting,
        [StringValue("Arm Wrestling")]ArmWrestling,
        [StringValue("Wrestling")]Wrestling,
        [StringValue("Flower Picking")]FlowerPicking,
        [StringValue("Orientation")]Orientation,
        [StringValue("Fencing")]Fencing,
        [StringValue("Acid")]Acid,
        [StringValue("Video Games")]VideoGames
    }

    public class StringValue : System.Attribute
    {
        private string _value;
        public string Value { get { return _value; } }
        public StringValue(string value) => _value = value;

        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();

            FieldInfo fi = type.GetField(value.ToString());
            StringValue[] attrs =
            fi.GetCustomAttributes(typeof(StringValue), false) as StringValue[];
            
            if(attrs.Length > 0) output = attrs[0].Value;
            return output;
        }
    }

}